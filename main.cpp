#include <iostream>

using namespace std;

class InvalidDateException {
};

class CDate {
    uint64_t secs;
    CDate(int y, int m, int d);
    CDate &operator+(int d);
    CDate &operator-(int d);
    CDate &operator++(int);
    CDate &operator++();
    CDate &operator--(int);
    CDate &operator--();
    friend int operator-(CDate date1, CDate date2);

    friend int operator-(CDate date1, CDate date2);

    friend int operator==(CDate date1, CDate date2);
    friend int operator!=(CDate date1, CDate date2);
    friend int operator>=(CDate date1, CDate date2);
    friend int operator<=(CDate date1, CDate date2);
    friend int operator>(CDate date1, CDate date2);
    friend int operator<(CDate date1, CDate date2);
};

bool operator==(CDate date1, CDate date2);
bool operator!=(CDate date1, CDate date2);
bool operator>=(CDate date1, CDate date2);
bool operator<=(CDate date1, CDate date2);
bool operator>(CDate date1, CDate date2);
bool operator<(CDate date1, CDate date2);

int main() {

    return 0;
}